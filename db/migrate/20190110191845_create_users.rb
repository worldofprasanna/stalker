class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :twitter_handle
      t.string :medium_handle
      t.string :github_handle

      t.timestamps
    end
  end
end
